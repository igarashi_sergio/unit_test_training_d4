//
//  CSDJsonParserHelper.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/15/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDJsonParserHelper: NSObject {
    
    static func loadJsonFrom(file:String) ->AnyObject?{
        
        if let path = NSBundle.init(forClass: CSDJsonParserHelper.self).pathForResource(file, ofType: "json")
        {
            if let jsonData = NSData(contentsOfFile: path)
            {
                do{
                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)
                    return jsonResult
                    
                }catch{
                }
            }
        }
        return nil
    }

}
