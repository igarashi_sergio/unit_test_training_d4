//
//  RACObserve.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/16/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import Foundation
import ReactiveCocoa

// replaces the RACObserve macro
func RACObserve(target: NSObject!, keyPath: String) -> RACSignal  {
    return target.rac_valuesForKeyPath(keyPath, observer: target)
}
