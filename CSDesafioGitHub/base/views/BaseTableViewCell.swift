//
//  BaseTableViewCell.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/5/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

protocol BaseTableViewCell:BaseReactiveView {
    func getCellIdentifier() -> String
}
