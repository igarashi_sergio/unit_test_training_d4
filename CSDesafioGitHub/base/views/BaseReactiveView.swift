//
//  BaseReactiveView.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/4/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

protocol BaseReactiveView {
    func bindViewModel(viewModel:AnyObject)
}