//
//  CSDLoadingCell.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/16/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDLoadingCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityIndicator.startAnimating()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
