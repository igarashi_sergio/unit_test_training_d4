//
//  CSDLoadingViewController.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/17/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDLoadingViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var completionBlock : (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = true
        self.activityIndicator.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
