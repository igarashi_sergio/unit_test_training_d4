//
//  CSDNavigationManager.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/17/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDNavigationManager: NSObject {
    static let sharedInstance = CSDNavigationManager()
    private let mainStoryboard = UIStoryboard()
    
    var navigator : UINavigationController? = nil

    private override init(){
        
    }
    
    private func pushController(controller:UIViewController){
        self.navigator?.pushViewController(controller, animated: false)
    }
    
    func popController(){
        self.navigator?.popViewControllerAnimated(true)
    }
    
    func pushLoadingScreen(){
        let loadingController = mainStoryboard.instantiateViewControllerWithIdentifier("CSDLoadingViewController")
        self.pushController(loadingController)
    }
    
    func pushPullsScreen(repository: CSDRepository){
        let pullsController = mainStoryboard.instantiateViewControllerWithIdentifier("CSDPullRequestsViewController") as! CSDPullRequestsViewController
        
        pullsController.repository = repository
        self.pushController(pullsController)
    }
    
}
