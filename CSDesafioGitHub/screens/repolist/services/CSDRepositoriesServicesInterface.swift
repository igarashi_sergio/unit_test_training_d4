//
//  CSDRepositoriesServicesInterface.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/9/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//


protocol CSDRepositoriesServicesInterface {
    func getRepositories(pageNumber: Int,returnHandler: (repositories:[CSDRepository]) -> Void)
}