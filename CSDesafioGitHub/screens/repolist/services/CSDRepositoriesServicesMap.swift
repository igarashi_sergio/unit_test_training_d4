//
//  CSDRepositoriesServicesMap.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/12/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import Foundation
import ObjectMapper

struct CSDRepositoriesServicesMap {
    
    static let reposURL = "https://api.github.com/search/repositories"
    
    static func parseRepositoriesResponse(response: AnyObject) -> CSDRepositoriesServicesResponse? {
        return Mapper<CSDRepositoriesServicesResponse>().map(response)
    }
}