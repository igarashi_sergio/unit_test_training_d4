//
//  CSDRepositoriesServicesImpl.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/9/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//
//        SignalProducer<[CSDRepositoryCellViewModel],NoError>.attempt { () -> Result<[CSDRepositoryCellViewModel], NoError> in
//
//            return Result<[CSDRepositoryCellViewModel],NoError>(CSDRepositoryCellViewModel.getMock())
//
//        }.start(){
//            event in
//            if let repos = event.value{
//            returnHandler(repositories: repos)
//            }
//        }
//

import ReactiveCocoa
import Result
import Alamofire
import ObjectMapper

class CSDRepositoriesServicesImpl: NSObject,CSDRepositoriesServicesInterface {

    func getRepositories(pageNumber: Int, returnHandler: (repositories:[CSDRepository]) -> Void) {
        
        var stringPage = "0"
        if(pageNumber > 0){
            stringPage = "\(pageNumber)"
        }
        
        let params = ["q":"language:Java",
                      "sort":"stars",
                      "page":"\(stringPage)"]
        
        
        Alamofire.request(.GET, CSDRepositoriesServicesMap.reposURL, parameters: params)
            .responseJSON { response in
                if let serviceReturn = response.result.value{
                    self.receivedResponseFromReposService(serviceReturn,returnHandler: returnHandler)
                }
        }
        
    }

    
    private func receivedResponseFromReposService(response:AnyObject, returnHandler:(repositories:[CSDRepository]) -> Void){
        if let parsedRepositories = CSDRepositoriesServicesMap.parseRepositoriesResponse(response){
            if let repositories = parsedRepositories.repositories{
                returnHandler(repositories:repositories)
            }
        }
    }
    
}
