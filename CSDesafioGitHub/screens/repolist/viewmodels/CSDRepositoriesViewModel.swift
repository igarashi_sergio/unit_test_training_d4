//
//  CSDRepositoriesViewModel.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/9/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ReactiveCocoa
import Result

class CSDRepositoriesViewModel: NSObject {

    var tableviewManager: CSDRepositoriesDatasource = CSDRepositoriesDatasource(repositories: [CSDRepository]())
    private var tableView:UITableView
    
    
    init(tableView: UITableView){
        self.tableView = tableView
        self.tableView.dataSource = self.tableviewManager
        self.tableView.delegate = self.tableviewManager
        super.init()
        
        RACObserve(self.tableviewManager, keyPath: "currentRepositoriesPage").subscribeNext {
            (object:AnyObject!) -> () in
                self.requestRepositories(CSDRepositoriesServicesImpl())
        }
    }
    
    func requestRepositories(repServicesImpl:CSDRepositoriesServicesInterface){
        repServicesImpl.getRepositories(self.tableviewManager.currentRepositoriesPage,returnHandler:self.loadRepositoriesList)
    }
    
    func loadRepositoriesList(repositories:[CSDRepository]){
        self.tableviewManager.repositories.appendContentsOf(repositories)
        self.tableView.reloadData()
    }
}

