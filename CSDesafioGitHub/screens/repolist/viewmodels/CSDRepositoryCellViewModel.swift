//
//  CSDRepositoryCellViewModel.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/4/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ReactiveCocoa

class CSDRepositoryCellViewModel: NSObject {
    
    var repoName = ""
    var repoDescription = ""
    var repoForks = ""
    var repoStars = ""
    var repoUsername = ""
    var repoAvatarURL = NSURL()
    
    private var _repository: CSDRepository? = nil
    var repository: CSDRepository? {
        get {
            return self._repository
        }
        set {
            self._repository = newValue
            if let name = newValue?.repoName{
                repoName = name
            }
            if let desc = newValue?.repoDescription{
                repoDescription = desc
            }
            if let forks = newValue?.repoForksCount{
                repoForks = String(forks)
            }
            if let stars = newValue?.repoStarsCount{
                repoStars = String(stars)
            }
            if let username = newValue?.repoOwner?.authorUsername{
                repoUsername = username
            }
            if let urlString = newValue?.repoOwner?.authorAvatarURL{
                if let url = NSURL(string:urlString){
                    repoAvatarURL = url
                }
            }
            
        }
    }
}
