//
//  CSDRepositoriesServicesResponse.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/11/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ObjectMapper

class CSDRepositoriesServicesResponse: NSObject,Mappable{

    
    var totalCount: Int?
    var incompleteResults: Bool?
    var repositories: [CSDRepository]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.totalCount <- map["total_count"]
        self.incompleteResults <- map["incomplete_results"]
        self.repositories <- map["items"]
    }
    
}
