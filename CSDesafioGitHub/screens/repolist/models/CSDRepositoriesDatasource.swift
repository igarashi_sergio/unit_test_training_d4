//
//  CSDRepositoriesDatasource.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/4/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.

import ReactiveCocoa
import Result
import UIKit

class CSDRepositoriesDatasource: NSObject,UITableViewDataSource,UITableViewDelegate {
    
    var repositories:[CSDRepository]
    dynamic var currentRepositoriesPage: Int = 1

    init(repositories:[CSDRepository]){
        self.repositories = repositories
        
    }
  
    //MARK: Datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //add 1 row to tableview for the loading view
        if(self.repositories.count > 0){
            return self.repositories.count + 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row == self.repositories.count){
            return self.dequeueLoadingCellFrom(tableView, cellForRowAtIndexPath: indexPath)
        }else{
            return self.dequeueRepositoryCellFrom(tableView, cellForRowAtIndexPath: indexPath)
        }
    }
    
    func dequeueRepositoryCellFrom(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> CSDRepositoryCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CSDRepositoryCell",forIndexPath: indexPath)
        if let baseCell = cell as? BaseTableViewCell{
            let cellViewModel = CSDRepositoryCellViewModel()
            cellViewModel.repository = self.repositories[indexPath.row]
            baseCell.bindViewModel(cellViewModel)
        }
        return cell as! CSDRepositoryCell
    }
    
    func dequeueLoadingCellFrom(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> CSDLoadingCell {
        return tableView.dequeueReusableCellWithIdentifier("CSDLoadingCell", forIndexPath: indexPath) as! CSDLoadingCell
    }
    
    //MARK: Delegate
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == self.repositories.count){
            self.currentRepositoriesPage = self.currentRepositoriesPage + 1
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let repo = self.repositories[indexPath.row]
        //CSDNavigationManager.sharedInstance.pushPullsScreen(repo)
    }
    
}
