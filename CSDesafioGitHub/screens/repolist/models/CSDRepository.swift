//
//  CSDRepository.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/12/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ObjectMapper

class CSDRepository: NSObject,Mappable {
    
    var id: Int?
    var repoName: String?
    var repoDescription: String?
    var repoStarsCount: Int?
    var repoForksCount: Int?
    var repoOwner: CSDRepositoryOwner?
    
    
    required init?(_ map: Map) {
        
    }
    
    override init() {
        
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.repoName <- map["full_name"]
        self.repoDescription <- map["description"]
        self.repoStarsCount <- map["stargazers_count"]
        self.repoForksCount <- map["forks_count"]
        self.repoOwner <- map["owner"]
    }
    
}
