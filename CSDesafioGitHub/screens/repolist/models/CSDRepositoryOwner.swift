//
//  CSDRepositoryOwner.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/12/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ObjectMapper

class CSDRepositoryOwner: NSObject,Mappable {
    
    var authorId: Int?
    var authorUsername: String?
    var authorName: String?
    var authorAvatarURL: String?
    
    required init?(_ map: Map) {
        
    }
    
    override init() {
        
    }
    
    func mapping(map: Map) {
        self.authorId <- map["id"]
        self.authorUsername <- map["login"]
        self.authorName <- map["login"]
        self.authorAvatarURL <- map["avatar_url"]
    }

}
