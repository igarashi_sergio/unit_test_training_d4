//
//  CSDRepositoriesViewController.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/4/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ReactiveCocoa
import Result
import Alamofire

class CSDRepositoriesViewController: UITableViewController {
    
    private var viewModel: CSDRepositoriesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Repositories"
        CSDNavigationManager.sharedInstance.navigator = self.navigationController
        self.configureViewModel()
    }
    
    private func configureViewModel(){
        self.viewModel = CSDRepositoriesViewModel(tableView: self.tableView)
    }
    
    

}


