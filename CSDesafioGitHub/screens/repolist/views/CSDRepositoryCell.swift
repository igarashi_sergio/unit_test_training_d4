//
//  CSDRepositoryCell.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/4/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import ReactiveCocoa
import SDWebImage

class CSDRepositoryCell: UITableViewCell,BaseTableViewCell {
    
    @IBOutlet weak var lblRepoName: UILabel!
    @IBOutlet weak var lblRepoDescription: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblStars: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    private var viewModel:CSDRepositoryCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgAvatar.layer.cornerRadius = 28
        self.imgAvatar.clipsToBounds = true
    }
    
    func bindViewModel(viewModel: AnyObject) {
        if let vm = viewModel as? CSDRepositoryCellViewModel{
            self.viewModel = vm
            self.lblRepoName.text = self.viewModel?.repoName
            self.lblRepoDescription.text = self.viewModel?.repoDescription
            self.lblForks.text = self.viewModel?.repoForks
            self.lblStars.text = self.viewModel?.repoStars
            self.lblUsername.text = self.viewModel?.repoUsername
            if let url = self.viewModel?.repoAvatarURL{
                self.loadAvatar(url)
            }
            
        }
    }
    
    private func loadAvatar(url:NSURL){
        if let placeHolderImage = UIImage(named:"avatar"){
            self.imgAvatar.sd_setImageWithURL(url, placeholderImage: placeHolderImage)
        }else{
            self.imgAvatar.sd_setImageWithURL(url)
        }
    }
    
    func getCellIdentifier() -> String {
        return String(CSDRepositoryCell.self)
    }

}
