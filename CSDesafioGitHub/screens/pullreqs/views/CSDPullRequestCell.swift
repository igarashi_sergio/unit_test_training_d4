//
//  CSDPullRequestCell.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/16/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDPullRequestCell: UITableViewCell {

    @IBOutlet weak var pullName: UILabel!
    @IBOutlet weak var pullDescription: UILabel!
    @IBOutlet weak var pullUsername: UILabel!
    @IBOutlet weak var pullUserAvatar: UIImageView!
    @IBOutlet weak var pullDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
