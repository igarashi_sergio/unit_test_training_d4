//
//  CSDPullRequestsViewController.swift
//  CSDesafioGitHub
//
//  Created by Sérgio Igarashi on 8/16/16.
//  Copyright © 2016 Sergio Igarashi. All rights reserved.
//

import UIKit

class CSDPullRequestsViewController: UITableViewController {
    
    var repository: CSDRepository?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
    }
    
    private func configNavigationBar(){
        if let repoName = self.repository?.repoName{
            self.title = repoName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
